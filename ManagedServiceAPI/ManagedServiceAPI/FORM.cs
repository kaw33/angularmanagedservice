//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class FORM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FORM()
        {
            this.FORMDATAs = new HashSet<FORMDATA>();
        }
    
        public int FormID { get; set; }
        public Nullable<System.Guid> AccountGUID { get; set; }
        public string Data { get; set; }
        public string FormName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FORMDATA> FORMDATAs { get; set; }
    }
}

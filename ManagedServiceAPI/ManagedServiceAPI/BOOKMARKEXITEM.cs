//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class BOOKMARKEXITEM
    {
        public int BookmarkExItemID { get; set; }
        public Nullable<int> BookmarkExID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TargetType { get; set; }
        public string Target { get; set; }
        public Nullable<bool> HasImage { get; set; }
        public string ImageURL { get; set; }
    }
}

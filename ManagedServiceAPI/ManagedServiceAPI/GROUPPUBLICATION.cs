//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class GROUPPUBLICATION
    {
        public System.Guid GroupPublicationGUID { get; set; }
        public Nullable<System.Guid> GroupGUID { get; set; }
        public Nullable<System.Guid> PublicationGUID { get; set; }
        public Nullable<bool> IsSupplement { get; set; }
        public string TextToDisplay { get; set; }
        public Nullable<int> WhatDayToShowSun1 { get; set; }
    
        public virtual GROUP GROUP { get; set; }
        public virtual PUBLICATION PUBLICATION { get; set; }
    }
}

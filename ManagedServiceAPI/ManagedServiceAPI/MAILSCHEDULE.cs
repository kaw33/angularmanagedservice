//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class MAILSCHEDULE
    {
        public int MailScheduleID { get; set; }
        public System.Guid AccountGUID { get; set; }
        public int MailTemplateID { get; set; }
        public Nullable<System.Guid> PublicationGUID { get; set; }
        public bool SendToAllPubs { get; set; }
        public Nullable<int> MailingListID { get; set; }
        public Nullable<int> ExclusionListID { get; set; }
        public int MailScheduleTypeID { get; set; }
        public Nullable<System.DateTime> LastRun { get; set; }
        public Nullable<System.DateTime> NextRun { get; set; }
        public Nullable<System.DateTime> TimeStarted { get; set; }
        public bool IsPaused { get; set; }
        public string MailServer { get; set; }
        public string MonTime { get; set; }
        public string TueTime { get; set; }
        public string WedTime { get; set; }
        public string ThuTime { get; set; }
        public string FriTime { get; set; }
        public string SatTime { get; set; }
        public string SunTime { get; set; }
        public bool AutomaticReports { get; set; }
        public string ReportAddress { get; set; }
        public Nullable<System.Guid> CheckForEdition { get; set; }
    
        public virtual ACCOUNT ACCOUNT { get; set; }
        public virtual MAILSCHEDULETYPE MAILSCHEDULETYPE { get; set; }
        public virtual MAILTEMPLATE MAILTEMPLATE { get; set; }
    }
}

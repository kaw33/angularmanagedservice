//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class XMLLINK
    {
        public int XMLLinkID { get; set; }
        public string Location { get; set; }
        public Nullable<System.Guid> EditionGUID { get; set; }
        public Nullable<System.Guid> PublicationGUID { get; set; }
        public Nullable<System.Guid> AccountGUID { get; set; }
        public Nullable<bool> HasCompleted { get; set; }
        public Nullable<bool> SystemCreated { get; set; }
    }
}

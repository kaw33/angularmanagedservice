﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagedServiceAPI.Models
{
    public class CHECKMODELDTO
    {
        public int CheckID { get; set; }
        public System.Guid EditionGUID { get; set; }
        public string PublicationName { get; set; }
        public Nullable<bool> ComicsAndPuzzles { get; set; }
        public Nullable<bool> Autolinks { get; set; }
        public Nullable<bool> Articles { get; set; }
        public string ArticleIssues { get; set; }
        public Nullable<System.DateTime> Completed { get; set; }
        public string HeadlineFixes { get; set; }
    }
}
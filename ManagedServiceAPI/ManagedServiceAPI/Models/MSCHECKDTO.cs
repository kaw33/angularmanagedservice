﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagedServiceAPI.Models
{
    public class MSCHECKDTO
    {
        public int PubID { get; set; }
        public System.Guid PublicationGUID { get; set; }
        public int CheckID { get; set; }
        public System.DateTime EditionDate { get; set; }
        public int UserID { get; set; }
        public Nullable<bool> ComicsAndPuzzles { get; set; }
        public Nullable<bool> Autolinks { get; set; }
        public Nullable<bool> Articles { get; set; }
        public string ArticleIssues { get; set; }
        public Nullable<System.DateTime> Completed { get; set; }
        public string HeadlineFixes { get; set; }
    }
}
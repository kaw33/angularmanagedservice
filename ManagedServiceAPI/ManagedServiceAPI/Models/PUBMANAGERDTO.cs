﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagedServiceAPI.Models
{
    public class PUBMANAGERDTO
    {
        public string PublicationName { get; set; }
        public int PubID { get; set; }
        public System.Guid PublicationGUID { get; set; }
        public int OrderID { get; set; }
        public string Name { get; set; }
        public Nullable<bool> ShowMonday { get; set; }
        public Nullable<bool> ShowTuesday { get; set; }
        public Nullable<bool> ShowWednesday { get; set; }
        public Nullable<bool> ShowThursday { get; set; }
        public Nullable<bool> ShowFriday { get; set; }
        public Nullable<bool> ShowSaturday { get; set; }
        public Nullable<bool> ShowSunday { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagedServiceAPI.Models
{
    public class OVERVIEWDTO
    {
        public System.DateTime ScheduledFor { get; set; }
        public Nullable<int> Progress { get; set; }
        public string PublicationName { get; set; }
        public System.Guid PublicationGUID { get; set; }
        public System.Guid EditionGUID { get; set; }
        public string EditionName { get; set; }
        public System.Guid AccountGUID { get; set; }


    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class TWITTERTWEET
    {
        public int TweetID { get; set; }
        public int TwitterAID { get; set; }
        public string TweetText { get; set; }
        public System.Guid PublicationGUID { get; set; }
        public bool UseEditionURL { get; set; }
        public bool UsePublicationURL { get; set; }
        public bool ASAP { get; set; }
        public bool OneTime { get; set; }
        public bool Daily { get; set; }
        public bool Weekly { get; set; }
        public bool Monthly { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> LastRun { get; set; }
    
        public virtual PUBLICATION PUBLICATION { get; set; }
        public virtual TWITTERACCOUNT TWITTERACCOUNT { get; set; }
    }
}

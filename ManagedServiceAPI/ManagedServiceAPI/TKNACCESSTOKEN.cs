//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class TKNACCESSTOKEN
    {
        public int ID { get; set; }
        public System.Guid AccountGUID { get; set; }
        public string Reason { get; set; }
        public System.DateTime EndDate { get; set; }
        public string Token { get; set; }
        public string GeneratedBy { get; set; }
        public System.DateTime GeneratedOn { get; set; }
    }
}

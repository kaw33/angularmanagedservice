//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class SYSTEMTRIGGER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SYSTEMTRIGGER()
        {
            this.SYSTEMALERTMESSAGEDATAs = new HashSet<SYSTEMALERTMESSAGEDATA>();
        }
    
        public int SystemTriggerID { get; set; }
        public Nullable<int> SystemAlertID { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public Nullable<System.DateTime> DateFinished { get; set; }
        public string Reason { get; set; }
        public Nullable<System.Guid> EditionGUID { get; set; }
        public Nullable<int> SystemAlertDataID { get; set; }
        public string OutputFile { get; set; }
        public string Action { get; set; }
        public string Number { get; set; }
    
        public virtual SYSTEMALERT SYSTEMALERT { get; set; }
        public virtual SYSTEMALERTDATA SYSTEMALERTDATA { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SYSTEMALERTMESSAGEDATA> SYSTEMALERTMESSAGEDATAs { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ManagedServiceAPI;
using ManagedServiceAPI.Models;
using System.Collections;
using System.Web.Http.Cors;

namespace ManagedServiceAPI.Controllers
{
    public class MSPubManager
    {
        public List<PUBMANAGERDTO> msPubManager { get; set; }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class pubmanagerController : ApiController
    {
        private PAGESUITEEntities db = new PAGESUITEEntities();
        private SUPPORTEntities msdb = new SUPPORTEntities();

        // GET: api/pubmanager
        [ResponseType(typeof(PUBMANAGERDTO))]
        public async Task<IHttpActionResult> GetPubs()
        {
            using (var db7 = new PAGESUITEEntities())
            using (var db3 = new SUPPORTEntities())
            {
                var psdb = db7.PUBLICATIONs.AsEnumerable();
                var msdbPubs = db3.MSPUBLICATIONs.AsEnumerable();
                var msdbOrder = db3.MSORDERs.AsEnumerable();

                MSPubManager mspm = new MSPubManager();

                mspm.msPubManager = (from mspub in msdbPubs
                                 join pspub in psdb on mspub.PublicationGUID equals pspub.PublicationGUID
                                 join msorder in msdbOrder on mspub.OrderID equals msorder.OrderID
                                     orderby pspub.Name ascending
                                     orderby mspub.OrderID ascending
                                 select new PUBMANAGERDTO
                                 {
                                     PublicationName = pspub.Name,
                                     PublicationGUID = mspub.PublicationGUID,
                                     PubID = mspub.PubID,
                                     OrderID = mspub.OrderID,
                                     Name = msorder.Name,
                                     ShowMonday = mspub.ShowMonday,
                                     ShowTuesday = mspub.ShowTuesday,
                                     ShowWednesday = mspub.ShowWednesday,
                                     ShowThursday = mspub.ShowThursday,
                                     ShowFriday = mspub.ShowFriday,
                                     ShowSaturday = mspub.ShowSaturday,
                                     ShowSunday = mspub.ShowSunday,

                                 }).ToList();
                if (mspm == null)
                {
                    return NotFound();
                }

                return Ok(mspm);

            }
        }


        // PUT: api/EDITION/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEDITION(Guid id, EDITION eDITION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eDITION.EditionGUID)
            {
                return BadRequest();
            }

            db.Entry(eDITION).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EDITIONExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EDITION
        [ResponseType(typeof(EDITION))]
        public async Task<IHttpActionResult> PostEDITION(EDITION eDITION)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EDITIONs.Add(eDITION);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EDITIONExists(eDITION.EditionGUID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = eDITION.EditionGUID }, eDITION);
        }

        // DELETE: api/EDITION/5
        [ResponseType(typeof(EDITION))]
        public async Task<IHttpActionResult> DeleteEDITION(Guid id)
        {
            EDITION eDITION = await db.EDITIONs.FindAsync(id);
            if (eDITION == null)
            {
                return NotFound();
            }

            db.EDITIONs.Remove(eDITION);
            await db.SaveChangesAsync();

            return Ok(eDITION);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EDITIONExists(Guid id)
        {
            return db.EDITIONs.Count(e => e.EditionGUID == id) > 0;
        }
    }
}
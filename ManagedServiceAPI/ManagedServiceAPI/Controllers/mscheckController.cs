﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ManagedServiceAPI;
using ManagedServiceAPI.Models;
using System.Web.Http.Cors;

namespace ManagedServiceAPI.Controllers
{
    public class GetMSSCheck
    {
        public List<MSCHECKDTO> MSCheck { get; set; }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class mscheckController : ApiController
    {
        private SUPPORTEntities db = new SUPPORTEntities();


        // GET: api/mscheck
        public GetMSSCheck Get()
        {
            //create an instance of AlbumResponse
            GetMSSCheck mschk = new GetMSSCheck();

            try
            {
                DateTime endDate = DateTime.Today;
                DayOfWeek todayDOW = DateTime.Today.DayOfWeek;

                switch (todayDOW)
                {
                    case DayOfWeek.Monday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowMonday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                    case DayOfWeek.Tuesday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowTuesday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();

                        }
                        break;
                    case DayOfWeek.Wednesday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowWednesday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                    case DayOfWeek.Thursday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowThursday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                    case DayOfWeek.Friday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowFriday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                    case DayOfWeek.Saturday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowSaturday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                    case DayOfWeek.Sunday:
                        {
                            List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowSunday == true).ToList();
                            List<Guid> publications = publicationsToday.Select(y => y.PublicationGUID).ToList();
                        }
                        break;
                }


                mschk.MSCheck = (from mspub in db.MSPUBLICATIONs
                                 join mscheck in db.MSUSERTOCHECKs on mspub.PubID equals mscheck.PubID
                                where mscheck.EditionDate >= endDate
                                orderby mspub.PubID ascending
                                select new MSCHECKDTO
                                {
                                    PubID = mspub.PubID,
                                    PublicationGUID = mspub.PublicationGUID,
                                    EditionDate = mscheck.EditionDate,
                                    CheckID = mscheck.CheckID,
                                    UserID = mscheck.UserID,
                                    ComicsAndPuzzles = mscheck.ComicsAndPuzzles,
                                    Autolinks = mscheck.Autolinks,
                                    Articles = mscheck.Articles,
                                    ArticleIssues = mscheck.ArticleIssues,
                                    HeadlineFixes = mscheck.HeadlineFixes,
                                    Completed = mscheck.Completed

                                }).ToList();
            }
            catch (Exception e)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred, please try again"),
                    ReasonPhrase = "Critical exception"
                });
            }
            return mschk;
        }

        // PUT: api/mscheck/checkid
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCheck( CHECKMODELDTO check)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest("Not a valid Model");

            }

            var queryCheck =  db.MSUSERTOCHECKs.Where(x => x.CheckID == check.CheckID).FirstOrDefault<MSUSERTOCHECK>();
            if(queryCheck != null)
            {

                    queryCheck.Articles = check.Articles;
                    queryCheck.Autolinks = check.Autolinks;
                    queryCheck.ComicsAndPuzzles = check.ComicsAndPuzzles;
                    queryCheck.HeadlineFixes = check.HeadlineFixes;
                    queryCheck.Completed = check.Completed;
                    db.SaveChanges();
                
            }
            else
            {
                return NotFound();
            }

            return Ok();
        }

        // POST: api/mscheck
        [ResponseType(typeof(MSPUBLICATION))]
        public async Task<IHttpActionResult> PostCheck()
        {
            DayOfWeek todayDOW = DateTime.Today.DayOfWeek;
            var MSuserID = 30904;
            var checkUserID = 30909;
            var date = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            switch(todayDOW)
            {
                case DayOfWeek.Monday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowMonday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Tuesday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowTuesday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Wednesday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowWednesday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Thursday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowThursday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Friday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowFriday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Saturday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowSaturday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
                case DayOfWeek.Sunday:
                    {
                        List<MSPUBLICATION> publicationsToday = db.MSPUBLICATIONs.Where(x => x.ShowSunday == true).ToList();
                        foreach (MSPUBLICATION p in publicationsToday)
                        {
                            if (p.OrderID != 4 && p.OrderID != 5)
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = MSuserID;
                                Console.WriteLine(chk);
                            }
                            else
                            {
                                MSUSERTOCHECK chk = new MSUSERTOCHECK();
                                chk.PubID = p.PubID;
                                chk.EditionDate = DateTime.Parse(date);
                                chk.UserID = checkUserID;
                                Console.WriteLine(chk);
                            }

                        }
                    }
                    break;
            }
            return Ok();
        }

    }
}
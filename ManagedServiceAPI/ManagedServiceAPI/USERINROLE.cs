//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class USERINROLE
    {
        public System.Guid UserInRoleGUID { get; set; }
        public System.Guid UserGUID { get; set; }
        public System.Guid RoleGUID { get; set; }
    
        public virtual ROLE ROLE { get; set; }
        public virtual USER USER { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class VOUCHERSCHEDULE
    {
        public System.Guid VoucherImportGuid { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public string Status { get; set; }
        public Nullable<decimal> ReductionAmount { get; set; }
        public Nullable<System.Guid> AccountGuid { get; set; }
        public Nullable<System.Guid> EditionGuid { get; set; }
        public Nullable<System.Guid> PublicationGuid { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> OriginalUseLimit { get; set; }
        public string FileName { get; set; }
    }
}

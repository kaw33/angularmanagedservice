//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class ARTICLEIMAGE
    {
        public int ArticleImageID { get; set; }
        public Nullable<int> ArticleID { get; set; }
        public string Path { get; set; }
        public string Caption { get; set; }
        public Nullable<int> OrderOf { get; set; }
        public string Name { get; set; }
    
        public virtual ARTICLE ARTICLE { get; set; }
    }
}

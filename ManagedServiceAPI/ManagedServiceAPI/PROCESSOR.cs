//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManagedServiceAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class PROCESSOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROCESSOR()
        {
            this.SCHEDULEs = new HashSet<SCHEDULE>();
        }
        [Newtonsoft.Json.JsonIgnore]
        public System.Guid ProcessorGUID { get; set; }
        public string Name { get; set; }
        [Newtonsoft.Json.JsonIgnore]
        public bool IsLive { get; set; }
        [Newtonsoft.Json.JsonIgnore]
        public Nullable<bool> IsEnabled { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Newtonsoft.Json.JsonIgnore]
        public virtual ICollection<SCHEDULE> SCHEDULEs { get; set; }
    }
}

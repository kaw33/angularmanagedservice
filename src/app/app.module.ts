import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './ngMaterial/material.module';
import { routing } from './routes/app-routing/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { JwtModule } from '@auth0/angular-jwt';
import { ErrorInterceptor } from '././helpers/error.interceptor';
import { JwtInterceptor } from './helpers/jwt.interceptor';

import { AppComponent } from './app.component';
import { OverviewComponent } from './overview/overview.component';
import { OverviewItemComponent } from './overview/overview-item/overview-item.component';
import { PubmanagerComponent } from './pubmanager/pubmanager.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PubmanagerItemComponent } from './pubmanager/pubmanager-item/pubmanager-item.component';
import { EditDialogComponent } from './overview/dialogs/edit.dialog/edit.dialog.component';
import { EditPublicationComponent } from './pubmanager/dialogs/edit/edit.component';
import { DeletePublicationComponent } from './pubmanager/dialogs/delete/delete.component';
import { CreatePublicationComponent } from './pubmanager/dialogs/create/create.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './authentication/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    OverviewItemComponent,
    PubmanagerComponent,
    NavigationComponent,
    PubmanagerItemComponent,
    EditDialogComponent,
    EditPublicationComponent,
    DeletePublicationComponent,
    CreatePublicationComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  
  entryComponents: [
     EditDialogComponent,
     EditPublicationComponent, 
     CreatePublicationComponent
    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { TestBed, inject } from '@angular/core/testing';

import { PubmanagerService } from './pubmanager.service';

describe('PubmanagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PubmanagerService]
    });
  });

  it('should be created', inject([PubmanagerService], (service: PubmanagerService) => {
    expect(service).toBeTruthy();
  }));
});

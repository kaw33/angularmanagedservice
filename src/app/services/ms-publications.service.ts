import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map, publish } from 'rxjs/operators';
import { MSCheck } from '../models/msCheck.model';
import { EditCheck } from '../models/editcheck.model'
import { MSPublication } from '../models/msPublication.model';
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class MsPublicationsService {
  private mspubs: MSPublication[] = [];
  private msCheck: MSCheck[] = [];
  private editionCheck: EditCheck[] = [];
  private mspubsUpdated = new Subject<MSPublication[]>();
  private msCheckUpdated = new Subject<MSCheck[]>();
  private editionID: string;


  constructor(private http: HttpClient) { }

  //GET api/overview
  /**
   * Gets the data from the PS DB and joins with the mscheck data from SUPPORT DB 
   */
  getMSPublications() {
    this.http
      .get<{ message: string, MSPubs: any }>(
        "http://localhost:1396/api/overview"
      )
      .pipe(map((mspubData) => {
        this.getMSCheck();
        const pubs = mspubData.MSPubs;
        const checks = this.msCheck;
        pubs.forEach(function (e) {
          var i = checks.filter(a => a.PublicationGUID == e.PublicationGUID).map(a => a);
          e.mschecks = i;
        });
        console.log(pubs);
        return pubs.map(checkPubs => {
          return {
            PublicationName: checkPubs.PublicationName,
            PublicationGUID: checkPubs.PublicationGUID,
            EditionGUID: checkPubs.EditionGUID,
            EditionName: checkPubs.EditionName,
            ScheduleProgress: checkPubs.Progress,
            ScheduledFor: checkPubs.ScheduledFor,
            AccountGUID: checkPubs.AccountGUID,
            mschecks: checkPubs.mschecks,
          };
        });
      }))
      .subscribe((transformedMSPubData) => {
        this.mspubs = transformedMSPubData;
        this.mspubs = this.mspubs;
        this.mspubsUpdated.next([...this.mspubs]);
        this.msCheckUpdated.next([...this.msCheck]);
      });
  }

  getAll() {
    this.getMSPublications();
  }

  //GET api/mscheck
  /**
   * Gets the data from the SUPPORT DB
   */
  getMSCheck() {
    this.http
      .get<{ message: string, MSCheck: any }>(
        "http://localhost:1396/api/mscheck"
      )
      .pipe(map((mschkData) => {
        return mschkData.MSCheck.map(MSCheck => {
          return {
            PubID: MSCheck.PubID,
            PublicationGUID: MSCheck.PublicationGUID,
            EditionDate: MSCheck.EditionDate,
            ComicsAndPuzzles: MSCheck.ComicsAndPuzzles,
            Autolinks: MSCheck.Autolinks,
            Articles: MSCheck.Autolinks,
            ArticleIssues: MSCheck.ArticleIssues,
            Completed: MSCheck.Completed,
            HeadlineFixes: MSCheck.HeadlineFixes,
            CheckID: MSCheck.CheckID
          };
        });
      }))
      .subscribe((transformedMSChkData) => {
        this.msCheck = transformedMSChkData;
        this.msCheckUpdated.next([...this.msCheck]);
      });
  }


  getEdition(editionGUID: string) {
    this.editionID = editionGUID;
    console.log({ ...this.mspubs.find(e => e.EditionGUID === this.editionID) })
    return { ...this.mspubs.find(e => e.EditionGUID === this.editionID) }
  }
  getMSPublicationUpdateListener() {
    return this.mspubsUpdated.asObservable();
  }
  getMSCheckUpdateListener() {
    return this.msCheckUpdated.asObservable();
  }

  //POST api/mscheck
  assignCheck() {
    
  }

  updateCheck(
    checkid: string,
    comics: boolean, 
    links: boolean,
    articles: boolean, 
    headlinefixes: string, 
    completed: string, 
    pubname: string, 
    editionGUID: string
    ) {
    const msCheckPub: EditCheck = {
      CheckID: checkid,
      ComicsAndPuzzles: comics,
      PublicationName: pubname,
      EditionGUID: editionGUID,
      Autolinks: links,
      Articles: articles,
      HeadlineFixes: headlinefixes,
      Completed: completed
    };
    this.http
      .put("http://localhost:1396/api/mscheck/" + checkid, msCheckPub)
      .subscribe(response => {
        const updatedEditionCheck = [...this.editionCheck];
        const oldCheckIndex = updatedEditionCheck.findIndex(e => e.EditionGUID === msCheckPub.EditionGUID);
        updatedEditionCheck[oldCheckIndex] = msCheckPub;
        this.editionCheck = updatedEditionCheck;
        this.mspubsUpdated.next([...this.mspubs]);
      });
  }
}


import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map, publish } from 'rxjs/operators';
import { PublicationManager } from '../models/pubManager.model';
import * as _ from "lodash";
@Injectable({
  providedIn: 'root'
})
export class PubmanagerService {
  private pubmanager: PublicationManager[] = [];
  private pubmanagerUpdated = new Subject<PublicationManager[]>();
  private id: number;

  constructor(private http: HttpClient) { }
  
  getPublications() {
    this.http
      .get<{ message: string, msPubManager: any }>(
        "http://localhost:1396/api/pubmanager"
      )
      .pipe(map((mspm) => {
        return mspm.msPubManager.map(msPubMgr => {
          return {
            PublicationGUID: msPubMgr.PublicationGUID,
            PublicationName: msPubMgr.PublicationName,
            PubID: msPubMgr.PubID,
            OrderID: msPubMgr.OrderID,
            Name: msPubMgr.Name,
            ShowMonday: msPubMgr.ShowMonday,
            ShowTuesday: msPubMgr.ShowTuesday,
            ShowWednesday: msPubMgr.ShowWednesday,
            ShowThursday: msPubMgr.ShowThursday,
            ShowFriday: msPubMgr.ShowFriday,
            ShowSaturday: msPubMgr.ShowSaturday,
            ShowSunday: msPubMgr.ShowSunday,
            AssigneeID: msPubMgr.AssigneeID,
            Recipients: msPubMgr.Recipients,
            TokenURL: msPubMgr.TokenURL,
            EditionURL: msPubMgr.EditionURL,
          };
        });
      }))
      .subscribe((transformedMSPMData) => {
        this.pubmanager = transformedMSPMData;
        this.pubmanagerUpdated.next([...this.pubmanager]);
      });
  }

  getMSPMUpdateListener() {
    return this.pubmanagerUpdated.asObservable();
  }

  getPublication(id: number) {
    this.id = id;
    console.log({ ...this.pubmanager.find(e => e.PubID === this.id) })
    return { ...this.pubmanager.find(e => e.PubID === this.id) }
  }

  updatePublication() {
    
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { MsPublicationsService } from './ms-publications.service';

describe('MsPublicationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MsPublicationsService]
    });
  });

  it('should be created', inject([MsPublicationsService], (service: MsPublicationsService) => {
    expect(service).toBeTruthy();
  }));
});

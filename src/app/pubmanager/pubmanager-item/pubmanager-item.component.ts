import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { PublicationManager } from '../../models/pubManager.model';
import { NewPublication } from '../../models/newpub.model';
import { PubmanagerService } from '../../services/pubmanager.service';
import { Observable } from 'rxjs/Observable';
import { MatDialog } from '@angular/material';
import { EditPublicationComponent } from '../dialogs/edit/edit.component';
import { CreatePublicationComponent } from '../dialogs/create/create.component'
import * as moment from 'moment';

@Component({
  selector: 'app-pubmanager-item',
  templateUrl: './pubmanager-item.component.html',
  styleUrls: ['./pubmanager-item.component.scss']
})
export class PubmanagerItemComponent implements OnInit {

  private pubmanager: PublicationManager[] = [];
  private pub: PublicationManager;
  private newPub: NewPublication;
  private pmSub: Subscription;
  private id: number;
  dataSource = new PMDataSource(this.pmservice);
  displayedColumns = ['OrderName','Title', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'buttons', 'deleteBtns'];
  private interval: any;

  constructor( private pmservice: PubmanagerService,
    private dialog: MatDialog,) { }
  ngOnInit() {
    this.pmservice.getPublications();
    this.pmSub = this.pmservice.getMSPMUpdateListener()
      .subscribe((pubMgr: PublicationManager[]) => {
        console.log(this.pubmanager);
        this.pubmanager = pubMgr;
      });
    this.refreshData();
    this.interval = setInterval(() => {
      this.refreshData();
    }, 10000);
  }

  ngOnDestroy() {
    this.pmSub.unsubscribe();
  }


  private refreshData() {
    this.pmservice.getPublications();

    this.pmSub = this.pmservice.getMSPMUpdateListener()
      .subscribe((pubMgr: PublicationManager[]) => {
        this.pubmanager = pubMgr;
      });
  }

  addNew(newPub: NewPublication){
    const dialogRef = this.dialog.open(CreatePublicationComponent, {
      data: {newPub: newPub}
    });
  }

  onEdit(id: number) {
    this.id = id;
    this.pub = this.pmservice.getPublication(this.id);
    console.log(this.pub)
    const dialogRef = this.dialog.open(EditPublicationComponent, {
      data: {
        PublicationGUID: this.pub.PublicationGUID,
        PublicationName: this.pub.PublicationName,
        PubID: this.pub.PubID,
        OrderID: this.pub.OrderID,
        Name: this.pub.Name,
        ShowMonday: this.pub.ShowMonday,
        ShowTuesday: this.pub.ShowTuesday,
        ShowWednesday: this.pub.ShowWednesday,
        ShowThursday: this.pub.ShowThursday,
        ShowFriday: this.pub.ShowFriday,
        ShowSaturday: this.pub.ShowSaturday,
        ShowSunday: this.pub.ShowSunday,
      }
    });
  }

}
export class PMDataSource extends DataSource<any>  {

  constructor(private pmservice: PubmanagerService) {
    super();
  }

  connect(): Observable<PublicationManager[]> {
    return this.pmservice.getMSPMUpdateListener();
  }

  disconnect() { }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubmanagerItemComponent } from './pubmanager-item.component';

describe('PubmanagerItemComponent', () => {
  let component: PubmanagerItemComponent;
  let fixture: ComponentFixture<PubmanagerItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubmanagerItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubmanagerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

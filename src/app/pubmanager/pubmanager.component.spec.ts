import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubmanagerComponent } from './pubmanager.component';

describe('PubmanagerComponent', () => {
  let component: PubmanagerComponent;
  let fixture: ComponentFixture<PubmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

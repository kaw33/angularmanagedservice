import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PubmanagerService } from '../../../services/pubmanager.service';
import { FormGroup, FormControl, NgForm, Validators } from '@angular/forms';
import { PublicationManager } from '../../../models/pubManager.model';
import { ActivatedRoute, ParamMap } from '@angular/router';
export interface SelectOrderID {
  value: number;
  viewValue: string;
}
export interface SelectAssigneeID {
  value: number;
  viewValue: string;
}

/**
 * Notes:
 * Needs an assignee table in order to seperate groups and assignees of pubs.
 * Needs a table with all email recepients and the corresponding publicationguids, this is to avoid hard coding them in.
 */
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreatePublicationComponent implements OnInit {
  mspm: PublicationManager;
  publicationGUID: string;
  order: SelectOrderID[] = [
    //Sets the group for the title
    {value: 1, viewValue: 'Early Title'},
    {value: 2, viewValue: 'Chicago Tribune'},
    {value: 3, viewValue: 'LA Times'},
    {value: 4, viewValue: 'Mediahuis'},
    {value: 5, viewValue: 'Paddock'},
    {value: 6, viewValue: 'Dallas'},
  ];
  assignee: SelectOrderID[] = [
    //Needs a new table to support this so we can retain order so pubs can be grouped
    {value: 1, viewValue: '7Con Title'},
    {value: 2, viewValue: 'PS Check'},
  ];
  name: string;
  showMonday: boolean;
  showTuesday: boolean;
  showWednesday: boolean;
  showThursday: boolean;
  showFriday: boolean;
  showSaturday: boolean;
  showSunday: boolean;
  assignees: string;
  recipents: number;
  constructor(
    public dialogRef: MatDialogRef<CreatePublicationComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: PublicationManager,
    public pubMgrService: PubmanagerService,
    public route: ActivatedRoute
  ) { }

  createPubForm = new FormGroup({
    pubGUIDCtrl: new FormControl(''),
    orderCtrl: new FormControl(''),
    mondayCtrl: new FormControl(''),
    tuesdayCtrl: new FormControl(''),
    wednesdayCtrl: new FormControl(''),
    thursdayCtrl: new FormControl(''),
    fridayCtrl: new FormControl(''),
    saturdayCtrl: new FormControl(''),
    sundayCtrl: new FormControl(''),
    assigneeCtrl: new FormControl(''),
    recipientsCtrl: new FormControl('')

  })

  ngOnInit() {
  }

}

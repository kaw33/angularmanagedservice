import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from 'src/app/app.component';
import { OverviewComponent } from 'src/app/overview/overview.component';
import { PubmanagerComponent } from 'src/app/pubmanager/pubmanager.component';
import { LoginComponent } from 'src/app/authentication/login/login.component';
import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: AppComponent, canActivate:[AuthGuard] },
  { path: 'overview', component: OverviewComponent,canActivate:[AuthGuard]},
  { path: 'pubmanager', component: PubmanagerComponent,canActivate:[AuthGuard]},
  { path: 'login', component: LoginComponent},
      // otherwise redirect to home
      { path: '**', redirectTo: '' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

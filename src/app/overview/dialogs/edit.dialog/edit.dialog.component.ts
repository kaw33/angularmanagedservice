import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MsPublicationsService } from '../../../services/ms-publications.service';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { MSPublication } from '../../../models/msPublication.model';
import { EditCheck } from '../../../models/editcheck.model';
import { ActivatedRoute, ParamMap } from '@angular/router';
import * as moment from 'moment';

export interface SelectCheckItemStatus {
  value: boolean;
  viewValue: string;
}

@Component({
  selector: 'app-edit.dialog',
  templateUrl: './edit.dialog.component.html',
  styleUrls: ['./edit.dialog.component.scss']
})
export class EditDialogComponent implements OnInit {
  mspublication: MSPublication;
  editionGUID: string;
  publicationName: string;
  completed: string;
  headlinefixes: string;
  comics: boolean;
  links: boolean;
  article: boolean;
  checkID: string;
  comicsandpuzzles: SelectCheckItemStatus[] = [
    {value: false, viewValue: 'Not Completed'},
    {value: true, viewValue: 'Completed'},
  ];
  autolinks: SelectCheckItemStatus[] = [
    {value: false, viewValue: 'Not Completed'},
    {value: true, viewValue: 'Completed'},
  ];
  articles: SelectCheckItemStatus[] = [
    {value: false, viewValue: 'Not Completed'},
    {value: true, viewValue: 'Completed'},
  ];

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: EditCheck,
    public msPublicationService: MsPublicationsService,
    public route: ActivatedRoute) { }

  updateCheckForm = new FormGroup({
    comicsCtrl: new FormControl(''),
    linksCtrl: new FormControl(''),
    articlesCtrl: new FormControl(''),
    headlinefixesCtrl: new FormControl(''),
    completedCtrl: new FormControl(''),
  });

  ngOnInit() {
    this.editionGUID = this.data.EditionGUID;
    this.publicationName = this.data.PublicationName;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmEdit() {
    const form = this.updateCheckForm;
    this.comics = this.updateCheckForm.controls['comicsCtrl'].value;
    this.links = this.updateCheckForm.controls['linksCtrl'].value;
    this.article = this.updateCheckForm.controls['articlesCtrl'].value;
    this.headlinefixes = this.updateCheckForm.controls['headlinefixesCtrl'].value;
    this.completed = moment().format('YYYY-MM-DD hh:mm:ss').toString();
    this.checkID = this.data.CheckID;
    // this.checkID = '27';
    this.editionGUID = this.data.EditionGUID;
    this.publicationName = this.data.PublicationName;
    
    if (form.invalid) {
      return;
    }
    if (this.article === false)
    {
      this.article = null;
    }
    if (this.links === false)
    {
      this.links = null;
    }
    if (this.comics === false)
    {
      this.comics = null;
    }
    console.log(this.comics, this.links, this.article, this.headlinefixes, this.completed, this.checkID, this.editionGUID, this.publicationName);
    this.msPublicationService.updateCheck(this.checkID, this.comics, this.links, this.article, this.headlinefixes, this.completed, this.publicationName,this.editionGUID);
  }




}

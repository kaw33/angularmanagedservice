import { Component, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { MSPublication } from '../../models/msPublication.model';
import { EditCheck } from '../../models/editcheck.model';
import { MsPublicationsService } from '../../services/ms-publications.service';
import { Observable } from 'rxjs/Observable';
import {MatDialog} from '@angular/material';
import {EditDialogComponent} from '../dialogs/edit.dialog/edit.dialog.component';
import { User } from '../../models/user.model';
import * as moment from 'moment';

@Component({
  selector: 'app-overview-item',
  templateUrl: './overview-item.component.html',
  styleUrls: ['./overview-item.component.scss']
})
export class OverviewItemComponent implements OnInit {
  private mspubs: MSPublication[] = [];
  private mspub: MSPublication;
  // private mspub: MSPublication;
  private mspubsSub: Subscription;
  dataSource = new OverviewDataSource(this.mspubservice);
  headElements = ['title', 'edition', 'progress', 'comlinks', 'articles', 'complete', 'menu'];
  displayedColumns = ['title', 'edition', 'progress', 'comlinks', 'articles', 'complete', 'menu'];
  private interval: any;
  private edid: string;
  users: User[] = [];
  constructor(
    private mspubservice: MsPublicationsService,
    private dialog: MatDialog,) { }

  ngOnInit() {
    this.mspubservice.getAll();
    this.mspubsSub = this.mspubservice.getMSPublicationUpdateListener()
      .subscribe((mspubs: MSPublication[]) => {
        console.log(this.mspubs);
        this.mspubs = mspubs;
      });
    this.refreshData();
    this.interval = setInterval(() => {
      this.refreshData();
    }, 5000);
  }

  ngOnDestroy() {
    this.mspubsSub.unsubscribe();
  }


  private refreshData() {
    this.mspubservice.getMSPublications()

    this.mspubsSub = this.mspubservice.getMSPublicationUpdateListener()
      .subscribe((mspubs: MSPublication[]) => {
        this.mspubs = mspubs;
      });
  }

  showElement(b: boolean) {
    if (b === true) {
      return true;
    }
    return false;
  }
  onEdit(edGUID: string) {
    this.edid = edGUID;
    this.mspub = this.mspubservice.getEdition(this.edid);
    console.log(this.mspub)
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: {
        CheckID: this.mspub.mschecks.map(x => x.CheckID).pop().toString(),
        EditionGUID: this.mspub.EditionGUID,
        PublicationName: this.mspub.PublicationName,
        ComicsAndPuzzles: this.mspub.mschecks.map(x => x.ComicsAndPuzzles),
        Autolinks: this.mspub.mschecks.map(x => x.Autolinks),
        HeadlineFixes: this.mspub.mschecks.map(x => x.HeadlineFixes),
      }
    });
  }

}
export class OverviewDataSource extends DataSource<any>  {

  constructor(private mspubservice: MsPublicationsService) {
    super();
  }

  connect(): Observable<MSPublication[]> {
    return this.mspubservice.getMSPublicationUpdateListener();
  }

  disconnect() { }
}


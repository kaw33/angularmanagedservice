import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MsPublicationsService } from '../services/ms-publications.service';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  private userid;
  constructor(
    private mspubservice: MsPublicationsService,
    private authservice: AuthenticationService,
    private route: ActivatedRoute ) { }

    ngOnInit() {
      this.userid = this.authservice.currentUser;
      console.log(this.userid);
      // Capture the access token and code
      this.route
          .queryParams
          .subscribe(params => {
              this.userid = params['#access_token'];
          });
        }
}

export interface EditCheck {
    CheckID: string,
    PublicationName: string,
    ComicsAndPuzzles: boolean,
    EditionGUID: string,
    Autolinks: boolean,
    Articles: boolean,
    HeadlineFixes: string,
    Completed: string,
}
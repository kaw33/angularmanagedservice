export interface PublicationManager {
    PublicationGUID: string,
    PublicationName: string,
    PubID: number,
    OrderID: number,
    Name: string,
    ShowMonday: boolean,
    ShowTuesday: boolean,
    ShowWednesday: boolean,
    ShowThursday: boolean,
    ShowFriday: boolean,
    ShowSaturday: boolean,
    ShowSunday: boolean,
    AssigneeID: number,
    Recipients: string,
    TokenURL: string,
    EditionURL: string,

}
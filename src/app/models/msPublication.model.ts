import{MSCheck} from './msCheck.model'
export interface MSPublication {
    AccountGUID: string,
    PublicationName: string,
    PublicationGUID: string,
    EditionGUID: string,
    EditionName: string,
    ScheduleProgress: number,
    ScheduledFor: Date,
    mschecks: MSCheck[],

    
}
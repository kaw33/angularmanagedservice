export interface NewPublication {
    PublicationGUID: string,
    OrderID: number,
    ShowMonday: boolean,
    ShowTuesday: boolean,
    ShowWednesday: boolean,
    ShowThursday: boolean,
    ShowFriday: boolean,
    ShowSaturday: boolean,
    ShowSunday: boolean,
    AssigneeID: number,
    Recipients: string,

}
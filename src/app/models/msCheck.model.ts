import {MSPublication} from './msPublication.model';
export interface MSCheck {
    PublicationGUID: string,
    EditionDate: string,
    ComicsAndPuzzles: boolean,
    Autolinks: boolean,
    Articles: boolean,
    ArticleIssues: boolean,
    Completed: string,
    HeadlineFixes: string,
    CheckID: string,
    
}